import {Injectable, Provider} from '@angular/core';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {BehaviorSubject, Observable} from 'rxjs';
import {root} from 'rxjs/internal-compatibility';
import {Storage} from '@ionic/storage';
import {AngularFireDatabase} from '@angular/fire/database';
import {ChefProjetService} from '../chefprojet/chef-projet.service';
import {Router} from '@angular/router';

@Injectable({providedIn: "root"})
export class AuthenticateService {

  public user: Observable<firebase.User>;
  private _userDetails: firebase.User = null;

private    authState = new BehaviorSubject(false);

  constructor(private _firebaseAuth: AngularFireAuth,
              private router:Router,
              private service:ChefProjetService,private storage: Storage,    private db: AngularFireDatabase,
  ) {

      this.user = _firebaseAuth.authState;
      this.user.subscribe(
          (user) => {
              if (user) {
                  this._userDetails = user;
                  console.log(this._userDetails);
              }
              else {
                  this._userDetails = null;
              }
          }
      );


  }
    get currentUserObservable(): any {
        return this._firebaseAuth.auth
    }

    get authenticated(): boolean {
        return this.authState !== null;
    }


    registerUser(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
          .then(

              res =>{ resolve(res);

this._firebaseAuth.auth.currentUser.updateProfile({displayName:value.fullName});
                  if(value.role.toString().toLowerCase()==="dev") {
                      this.db.database.ref('users/dev/'+this._firebaseAuth.auth.currentUser.uid).set({...value,...this._firebaseAuth.auth.currentUser.toJSON()});
                  }else   if(value.role.toString().toLowerCase()==="chef") {
                      this.db.database.ref('users/chefprojet/'+this._firebaseAuth.auth.currentUser.uid).set({...value,...this._firebaseAuth.auth.currentUser.toJSON()});
                  }
                  else   if(value.role.toString().toLowerCase()==="client") {

                      this.db.database.ref('users/clients/'+this._firebaseAuth.auth.currentUser.uid).set({...value,...this._firebaseAuth.auth.currentUser.toJSON()});
                  }

                  localStorage.setItem('currentUser', JSON.stringify({...value,...this._firebaseAuth.auth.currentUser.toJSON()}));

              },
              err => reject(err));
    });
  }

  loginUser(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
          .then(
              (res:any) => {resolve(res);
                  switch (value.role) {
                      case "dev":this.service.getDev(this._userDetails.uid).on("value",data=>{
                          console.log(data.val());
                          localStorage.setItem('currentUser',JSON.stringify(data.val()));

this.router.navigate(["/dashboard/dev"])
                      }); break;
                      case "client":this.service.getChefProjectById(this._userDetails.uid).subscribe((data:any)=>{

                          localStorage.setItem('currentUser', (JSON.stringify(JSON.parse(JSON.stringify(data)))));


                      });break;
                      case "chef":this.service.getChef(this._userDetails.uid).on("value",data=>{
                          console.log(data.val());
                          localStorage.setItem('currentUser',JSON.stringify(data.val()));
                      this.router.navigate(['/dashboard/chef-projet'])


                      });break;

                  }


              },
              err => reject(err))
    });
  }

  logoutUser() {
    this.user=null;
    return new Promise((resolve, reject) => {
      if(firebase.auth().currentUser){
        firebase.auth().signOut()
            .then(() => {
              console.log('LOG Out');
              resolve();
            }).catch((error) => {
          reject();
        });
      }
    });
  }

    isLoggedIn() {
      if(localStorage.getItem('currentUser'))
      {
          this.authState.next(true);
      }
                    this.authState.next(true);



    }
    isAuthenticated() {
        return this.authState.value;
    }

    logout() {
      localStorage.remove('currentUser').then(()=>{
          this._firebaseAuth.auth.signOut();

      });

    }
    getUserName(){
    return  JSON.parse( localStorage.getItem("currentUser"))
    }
    updatePassword(value){
      return this._firebaseAuth.auth.sendPasswordResetEmail(value);
    }

 userId()
{
   let uid=JSON.parse( localStorage.getItem('currentUser'));


    return  uid.uid;
}


}

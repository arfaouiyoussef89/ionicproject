import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import * as firebase from 'firebase';

@Injectable({
    providedIn: 'root'
})
export class DevService {

    constructor(private db:AngularFireDatabase) { }

    public getProject(value){
        return   this.db.list('projects', ref => ref.orderByChild("team/"+value).equalTo(true))



    }
    public getProjectclient(value) {
        let ref= this.db.list('projects',ref=>ref.orderByChild("team/-LwhQ6VzstBMzek4Lo2x").equalTo(true));
        console.log(ref.valueChanges().subscribe(res=>console.log(res)))}
    public createProject(value){
        return     this.db.list("projects").push(value)

    }
    public getDevByIds(id){

        this.db.list('users/dev/'+id).snapshotChanges();
    }
    public updateProgression(id,tacheId,value){
        return    this.db.database.ref("projects/"+id+"/taches/"+tacheId).update({progress:value})
    }
    public  deleteTeam(id,tacheId,value){
        return    this.db.database.ref("projects/"+id+"/taches/"+tacheId+"/team/devId/"+value).remove();

    }

    public  getTacheById(id,devId){
        console.log("tedy")
console.log(id)
        console.log(devId)

        return   this.db.list("projects/"+id+"/taches", ref => ref.orderByChild("team/devId/"+devId).equalTo(true))


    }



    public getProjectById(id){
        return this.db.database.ref('projects/'+id);
    }
    public getTache(id){
        return  this.db.list('projects/'+id+'/taches').snapshotChanges();
    }
    public getAllReunion(id,uid){
        return  this.db.list('projects/'+id+'/reunion',ref => ref.orderByChild("team/"+uid).equalTo(true)).snapshotChanges();
    }

    public getAllChatRoom(id,uid){
        return  this.db.list('projects/'+id+'/chatRooms',ref => ref.orderByChild("team/"+uid).equalTo(true)).snapshotChanges();
    }
    public getMessageChatRooms(id,idRoom){
        return    this.db.list('projects/'+id+"/chatRooms/"+idRoom+'/messages').valueChanges()
    }
    public addChatMessage(id,idRoom,value)
    {
        return this.db.database.ref('projects/'+id+'/chatRooms/'+idRoom+'/messages').push(value)
    }
    public addTache(id,value){
        return   this.db.database.ref('projects/'+id+'/taches').push(value);
    }
    public updateTache(id,tacheId,value){
        return  this.db.database.ref('projects/'+id+'/taches/'+tacheId).update(value);
    }
    public updateProjet(id,value){
        return  this.db.database.ref('projects/'+id).update(value);
    }
    public deleteProjet(id){
        return this.db.database.ref('projects/'+id).remove();
    }


    public  addReunion(id,data){
        this.db.database.ref('projects/'+id+'/reunion').push(data);

    }

    public  getAllDev(){

        return this.db.list('users/dev').snapshotChanges();
    }

    public getDevById(id){
        return this.db.list('users/dev/'+id).snapshotChanges();

    }
    public addDevId(id,uid){
        return this.db.database.ref('users/dev/'+id).set(uid);

    }
    public  getAllClients(){

        return this.db.list('users/clients').snapshotChanges();
    }
    public addChatRoom(id,value){
        return    this.db.database.ref('projects/'+id+'/chatRooms').push(value);

    }
    public getChefProjectById(id){
        return  this.db.list('users/chefprojet/'+id).snapshotChanges()

    }
    public updateProfile(id,value){
        return  this.db.database.ref('users/chefprojet/'+id).update(value);

    }


}

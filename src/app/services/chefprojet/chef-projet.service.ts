import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ChefProjetService {

  constructor(private db:AngularFireDatabase) { }

  public getProject(value){
    return   this.db.list('projects', ref => ref.orderByChild("chefprojet").equalTo(value))


  }
    public createProject(value){
      return     this.db.list("projects").push(value)

  }
public getDevByIds(id){

      this.db.list('users/dev/'+id).snapshotChanges();
}
  public affectTache(id,tacheId,value){
   return    this.db.database.ref("projects/"+id+"/taches/"+tacheId+"/team").set(value)
  }
    public  deleteTeam(id,tacheId,value){
        return    this.db.database.ref("projects/"+id+"/taches/"+tacheId+"/team/devId/"+value).remove();

    }


  public  getTacheById(id,tacheId){
      return this.db.database.ref("projects/"+id+"/taches/").child(tacheId);

  }
    public  deleteTacheById(id,tacheId){
        return this.db.database.ref("projects/"+id+"/taches/").child(tacheId).remove();

    }



  public getProjectById(id){
      return this.db.database.ref('projects/'+id);
  }
public getTache(id){
     return  this.db.list('projects/'+id+'/taches').snapshotChanges();
}
    public getAllReunion(id){
        return  this.db.list('projects/'+id+'/reunion').snapshotChanges();
    }

    public getAllChatRoom(id){
        return  this.db.list('projects/'+id+'/chatRooms').snapshotChanges();
    }
    public getMessageChatRooms(id,idRoom){
   return    this.db.list('projects/'+id+"/chatRooms/"+idRoom+'/messages').valueChanges()
    }
    public deleteChatRooms(id,idRoom){
        return    this.db.database.ref('projects/'+id+"/chatRooms").child(idRoom).remove()
    }

    public addChatMessage(id,idRoom,value)
    {
        return this.db.database.ref('projects/'+id+'/chatRooms/'+idRoom+'/messages').push(value)
    }
  public addTache(id,value){
    return   this.db.database.ref('projects/'+id+'/taches').push(value);
  }
    public updateTache(id,tacheId,value){
       return  this.db.database.ref('projects/'+id+'/taches/'+tacheId).update(value);
    }
    public updateProjet(id,value){
       return  this.db.database.ref('projects').child(id).set(value);
    }
    public deleteProjet(id){
        return this.db.database.ref('projects/'+id).remove();
    }


    public  addReunion(id,data){
        this.db.database.ref('projects/'+id+'/reunion').push(data);

    }

public  getAllDev(){

      return this.db.list('users/dev').snapshotChanges();
}

public getDevById(id){
      return this.db.list('users/dev/'+id).snapshotChanges();

}
    public getDev(id){
        return this.db.database.ref('users/dev/'+id);

    }
    public getChef(id){
        return this.db.database.ref('users/chefprojet/'+id);

    }

    public addDevId(id,uid){
        return this.db.database.ref('users/dev/'+id).set(uid);

    }
    public  getAllClients(){

        return this.db.list('users/clients').snapshotChanges();
    }
    public addChatRoom(id,value){
     return    this.db.database.ref('projects/'+id+'/chatRooms').push(value);

    }
    public getChefProjectById(id){
        return  this.db.list('users/chefprojet/'+id).snapshotChanges()

    }
    public updateProfile(id,value){
       return  this.db.database.ref('users/chefprojet/'+id).update(value);

    }


}

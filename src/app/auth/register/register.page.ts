import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NavController, MenuController, LoadingController, AlertController} from '@ionic/angular';
import {AuthenticateService} from '../../services/auth/authentication.service';
import {AngularFireDatabase} from '@angular/fire/database';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {AngularFireStorage} from '@angular/fire/storage';
import * as firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit
{
  public onRegisterForm: FormGroup;
     options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
         cameraDirection:this.camera.Direction.FRONT
    };

    captureDataUrl: string;
    alertCtrl: AlertController;

  constructor(
       alertCtrl: AlertController,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private authService: AuthenticateService,
    private db: AngularFireDatabase,
    private fr:AngularFireStorage,
    private camera: Camera,
    private afStorage: AngularFireStorage

  ) {
    this.alertCtrl = alertCtrl

}

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.onRegisterForm = this.formBuilder.group({
      'fullName': [null, Validators.compose([
        Validators.required
      ])],
      role:  [null, Validators.compose([
        Validators.required
      ])],
      image: [''],
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  async signUp(value) {
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });
    loader.present();

    this.authService.registerUser(value)
          .then(res => {
            loader.onWillDismiss().then(() => {

              this.navCtrl.navigateRoot('/dashboard/projets');
            });
          }, err => {
            console.log(err);
          });

  }
takeep(){

    this.camera.getPicture(this.options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.afStorage.upload('/upload/to/this-path', imageData);

    }, (err) => {
        // Handle error
    });

}
  // // //
  goToLogin() {
    this.navCtrl.navigateRoot('/');
  }




    capture() {
        const cameraOptions: CameraOptions = this.options;
        this.camera.getPicture(cameraOptions)
            .then((imageData) => {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64:

                this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
                alert(imageData);

                this.afStorage.upload("/images",imageData).then(()=>alert("success"),error=>{
alert('hi')
                    alert(error)
                }) ;           }, (err) => {
                // Handle error

            });
    } // End of capture camera


    upload() {
        // Create a timestamp as filename

    }

    showSuccesfulUploadAlert() {


}


}

import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";

@Injectable()
export class LoginActivate implements CanActivate {
    constructor( private router: Router) {}
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean>|Promise<boolean>|boolean {

let user=localStorage.getItem('currentUser');
console.log(user)
        if (user && JSON.parse(user )!==null) {
            console.log(user)
            switch (JSON.parse(user).role) {
                case "chef":this.router.navigate(['/dashboard/chef-projet'])
                    break;
                case "dev":this.router.navigate(['/dashboard/dev']);
                    break;
            }
            return false;
        }
        else
        return true;
    }
}

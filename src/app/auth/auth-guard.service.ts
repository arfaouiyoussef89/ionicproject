import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import AuthSettings = firebase.auth.AuthSettings;
import {AuthenticateService} from '../services/auth/authentication.service';

@Injectable({
    providedIn: "root"
})
export class AuthGuardService implements CanActivate {
    constructor(private router: Router,private auth:AuthenticateService) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        console.log(route);

        let user=localStorage.getItem('currentUser');


        if (!user || user.toString()===null) {
            this.router.navigate([""]);
            return false;
        }
else
        return true;
    }
}


import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import {DashboardCompoment} from './dashboard.compoment';
import {IonicModule} from '@ionic/angular';
import {AuthenticateService} from '../services/auth/authentication.service';
import {NgModule} from '@angular/core';
import {AvatarModule} from 'ngx-avatar';

@NgModule({
  declarations: [DashboardCompoment],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        IonicModule,
        AvatarModule
    ],
  providers:[AuthenticateService]
})
export class DashboardModule { }

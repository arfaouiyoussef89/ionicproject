import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Pages} from '../interfaces/pages';
import {AuthenticateService} from '../services/auth/authentication.service';
import {Observable} from 'rxjs';
import {ShareService} from '../services/shared/share.service';


@Component({
    selector: 'app-root',
    templateUrl: 'dashboard.html',
    styleUrls: ['./dashboard.scss']
})
export class DashboardCompoment {
    response = '';
    msg$: Observable<string>;


    public appPages: Array<Pages>;
private  user:any;
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public navCtrl: NavController,
        private auth: AuthenticateService ,
        private shared:ShareService,
    ) {
        this.user= JSON.parse( localStorage.getItem("currentUser"));

        this.shared.getResponse().subscribe(
                (data) => {
                    this.response = data;
                    this.user= JSON.parse( localStorage.getItem("currentUser"));

                }
            );



let uid=this.auth.userId();
switch (this.user.role) {
    case "chef":


        this.appPages = [
            {
                title: 'Projets',
                url: '/dashboard/chef-projet/',
                direct: 'root',
                icon: 'list'
            },


            {
                title: 'Statistique',
                url: '/dashboard/chef-projet/' + uid + '/state-projet',
                direct: 'forward',
                icon: 'stats'
            },
            {
                title: 'Parametrers',
                url: '/dashboard/chef-projet/' + uid + '/settings',
                direct: 'forward',
                icon: 'construct'
            }
        ];
break;
    case "dev":

        this.appPages = [
            {
                title: 'Projets',
                url: '/dashboard/dev/',
                direct: 'root',
                icon: 'list'
            },


            {
                title: 'Statistique',
                url: '/dashboard/dev/' + uid + '/state-projet',
                direct: 'forward',
                icon: 'stats'
            },
            {
                title: 'Parametrers',
                url: '/dashboard/dev/' + uid + '/settings',
                direct: 'forward',
                icon: 'construct'
            }
        ];





        break;
}
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        }).catch(() => {});
    }

    goToEditProgile() {
        this.navCtrl.navigateRoot('dashboard/settings');
    }

    logout() {
localStorage.clear();
    this.navCtrl.navigateRoot('/');}
}

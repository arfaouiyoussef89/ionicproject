import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {DevService} from '../../../../services/dev/dev.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';

@Component({
  selector: 'app-list-tache',
  templateUrl: './list-tache.component.html',
  styleUrls: ['./list-tache.component.scss']
})
export class ListTacheComponent implements OnInit {
    panelOpenState: boolean;
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three'];
    auto: any;
    private id: any;
    private taches=[];
    isaffected: any;
    private search: any;
    private progress: any ;

    constructor(private formBuilder: FormBuilder,private service:DevService,private auth:AuthenticateService,private route:ActivatedRoute,private router:Router) {
        this.id = this.route.snapshot.params.id;

        this.service.getTacheById(this.id,this.auth.userId()).snapshotChanges().subscribe(res=>{
            this.taches=(JSON.parse(JSON.stringify(res)));
            console.log(this.taches);
this.search=this.taches;
        })
    }
updateProgress(tacheid){
        this.service.updateProgression(this.id,tacheid,this.progress)


}
    filterItem(){
        if(!this.myControl.value){
            this.taches=this.search;
        } // when nothing has typed
        // @ts-ignore
        this.taches = Object.assign([], this.search).filter(
            item => item.payload.nameTache.toLowerCase().indexOf(this.myControl.value.toLowerCase()) > -1
        )}
    ngOnInit() {

    }

    setProgress(tache: any) {
        this.progress=!!tache.payload.progress? tache.payload.progress:'';
    }
}

import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';
import Swal from "sweetalert2";
import {NavController} from '@ionic/angular';
import {$e} from 'codelyzer/angular/styles/chars';

@Component({
  selector: 'app-add-projet',
  templateUrl: './add-projet.component.html',
  styleUrls: ['./add-projet.component.scss']
})
export class AddProjetComponent implements OnInit {
    private addProject : FormGroup;
clients:any;
dev:any;
    name: any;
    projects:any;
    private list=[];
    nameError: any;
project:any;
    private interessesG: FormArray;
    constructor( private formBuilder: FormBuilder,private service:ChefProjetService,private auth:AuthenticateService,
                 private nav:NavController) {
        this.addProject = this.formBuilder.group({
            nameProject: ['', Validators.required],
            description: [''],
            client: ['', Validators.required],
chefprojet:[this.auth.userId()],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],

            team:[]



        });

    this.service.getAllDev().pipe().subscribe((res:any)=>{
        this.dev=(JSON.parse(JSON.stringify(res)))
    }  );
        this.service.getAllClients().subscribe(res=>{
            this.clients=(JSON.parse(JSON.stringify(res)));
        }  );
    }

    logForm() {
        let  exists=null;
        let t=[];
        let s=[]

        t=this.addProject.controls["team"].value

t.map(res=>{
    s[res]=true;
})
        console.log(s)
        this.addProject.controls['team'].setValue(s);


     let   name=this.addProject.controls["nameProject"].value;
        this.service.getProject(this.auth.userId()).valueChanges().subscribe((res)=>{

             exists=res.filter((item:any)=> item.nameProject === name );
            if(exists.length > 0 )
            {

                this.nameError="project name already exists"
            }
            else {
                this.service.createProject(this.addProject.value).then(res=>{


                    Swal.fire({
                        title: 'add project  ',
                        text: 'success',
                        icon: 'success',
                    });
                    this.nav.back()

                });
            }
        });


    }
test(){
let exists=null;
    let   name=this.addProject.controls["nameProject"].value;

    this.service.getProject(this.auth.userId()).valueChanges().subscribe((res)=>{

        exists=res.filter((item:any)=> item.nameProject === name );
        console.log(exists.length);

        if(exists.length > 0 )
        {

            this.nameError="project name already exists"
        }

    });



}

   ngOnInit() {

  }

    onCheckChange(event) {
        console.log("ho");
    }
}

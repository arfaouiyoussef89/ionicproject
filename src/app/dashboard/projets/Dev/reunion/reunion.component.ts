import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DevService} from '../../../../services/dev/dev.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';

@Component({
  selector: 'app-reunion',
  templateUrl: './reunion.component.html',
  styleUrls: ['./reunion.component.scss']
})
export class ReunionComponent implements OnInit {
    panelOpenState: boolean;
    myControl = new FormControl();
    auto: any;
    private reunions= [];
    private id: any;
    private searchlist=[];

    constructor(private auth:AuthenticateService,private service:DevService,private route:ActivatedRoute,private router:Router) {
        this.id = this.route.snapshot.params.id;

        this.service.getAllReunion(this.id,this.auth.userId()).pipe().subscribe(res=>{
            this.reunions=(JSON.parse(JSON.stringify(res)));
            this.searchlist=this.reunions;
        })

    }
    filterItem(){
        if(!this.myControl.value){
            this.reunions=this.searchlist;
        } // when nothing has typed
        // @ts-ignore
        this.reunions = Object.assign([], this.searchlist).filter(
            item => item.payload.description.toLowerCase().indexOf(this.myControl.value.toLowerCase()) > -1
        )
    }


    search() {
        // @ts-ignore

       console.log( this.reunions.filter((item:any) => item.payload.description.toString().toLowerCase().includes(this.myControl.value))
       )
    }
    ngOnInit() {

    }


}

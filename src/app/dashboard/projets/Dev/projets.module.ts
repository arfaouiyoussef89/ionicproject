import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ProjetsComponent} from './projets.component';
import { ListProjetComponent } from './list-projet/list-projet.component';
import {IonicModule} from '@ionic/angular';
import { EditProjetComponent } from './edit-projet/edit-projet.component';
import {
    MatAutocompleteModule, MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule, MatInputModule
} from '@angular/material';
import {AvatarModule} from 'ngx-avatar';
import { AddProjetComponent } from './add-projet/add-projet.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AddTacheComponent } from './add-tache/add-tache.component';
import { EditTacheComponent } from './edit-tache/edit-tache.component';
import {TooltipsModule} from 'ionic-tooltips';
import { ListTacheComponent } from './list-tache/list-tache.component';
import { AffectTacheComponent } from './affect-tache/affect-tache.component';
import { AffectedDevComponent } from './affected-dev/affected-dev.component';
import {ReunionComponent} from './reunion/reunion.component';
import {AddReunionComponent} from './reunion/add-reunion/add-reunion.component';
import { ChatComponent } from './chat/chat.component';
import { ChatroomsComponent } from './chatrooms/chatrooms.component';
import { AddChatRoomComponent } from './add-chat-room/add-chat-room.component';
import {PickerModule} from '@ctrl/ngx-emoji-mart';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';


const routes: Routes = [
  {
    path: '',
    component: ProjetsComponent,
    children: [
      {
        path:'',
        component:ListProjetComponent,
      },
      {
        path:':id/edit-projet',
        component:EditProjetComponent,
      }
      ,{
            path:'add-projet',
            component:AddProjetComponent,
        },
        {
            path:':id/add-tache',
            component:AddTacheComponent,
        },
        {
            path:':id/taches',
            component:ListTacheComponent
        },
        {
            path:':ids/taches/:id/edit-tache',
            component:EditTacheComponent
        },
        {
            path:":ids/taches/:id/affected-dev",
            component:AffectedDevComponent
        },
        {
            path: ':id/state-projet' ,
            loadChildren: () => import('../../projets/dev/state-project/state-project.module')
                .then(m => m.StateProjectModule),
        },
        {
            path:':ids/taches/:id/affect-tache',
            component:AffectTacheComponent

        },{
            path: ':id/settings' ,
            loadChildren: () => import('../../projets/chefProjet/settings/settings.module')
                .then(m => m.SettingsPageModule),
        },{
            path:':id/reunion',
            component:ReunionComponent

        },
        {
            path:':id/add-reunion',
            component:AddReunionComponent
        }
,{
            path:':id/:ids/chat',
            component:ChatComponent
        }
,{
        path:":id/chat-rooms",
            component:ChatroomsComponent
        },
        {path:":id/add-chat-room",
        component:AddChatRoomComponent}


    ]
  },
];

@NgModule({
  declarations: [ProjetsComponent,ChatroomsComponent,ChatComponent,AddReunionComponent, ReunionComponent,ListProjetComponent, EditProjetComponent, AddProjetComponent, AddTacheComponent, EditTacheComponent, ListTacheComponent, AffectTacheComponent, AffectedDevComponent, ChatComponent, ChatroomsComponent, AddChatRoomComponent ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        IonicModule,
        MatExpansionModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatCardModule,
        MatDividerModule,
        MatIconModule,
        MatButtonModule,
        AvatarModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatInputModule,
        TooltipsModule,
        TooltipsModule,
        FormsModule,
        PickerModule,
        SweetAlert2Module,
        MatBadgeModule

    ]
})
export class ProjetsModule { }

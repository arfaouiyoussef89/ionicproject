import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DevService} from '../../../../services/dev/dev.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';

@Component({
  selector: 'app-chatrooms',
  templateUrl: './chatrooms.component.html',
  styleUrls: ['./chatrooms.component.scss']
})
export class ChatroomsComponent implements OnInit {

    panelOpenState: boolean;
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three'];
    auto: any;
    private id: any;
    private taches=[];
    isaffected: any;
    private search: any;

    constructor(private service:DevService,private auth:AuthenticateService,private route:ActivatedRoute,private router:Router) {
        this.id = this.route.snapshot.params.id;

        this.service.getAllChatRoom(this.id,this.auth.userId()).subscribe(res=>{
            this.taches=(JSON.parse(JSON.stringify(res)));
            this.search=this.taches;
            console.log(this.taches)
        })
    }
    affectedTache(){
        this.isaffected=true
    }
    notaffectedTache(){
        this.isaffected=false
    }

    listDev(key,val:any)
    {


        localStorage.setItem("dev-tache",JSON.stringify(val));

        this.router.navigate(["./"+key+"/affected-dev"],{ relativeTo: this.route });
    }
    filterItem(){
        if(!this.myControl.value){
            this.taches=this.search;
        } // when nothing has typed
        // @ts-ignore
        this.taches = Object.assign([], this.search).filter(
            item => item.payload.roomName.toLowerCase().indexOf(this.myControl.value.toLowerCase()) > -1
        )}
    ngOnInit() {

    }
}

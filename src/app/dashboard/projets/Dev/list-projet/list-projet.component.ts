import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {AlertController} from '@ionic/angular';
import Swal from 'sweetalert2';
import {AuthenticateService} from '../../../../services/auth/authentication.service';
import {DevService} from '../../../../services/dev/dev.service';

@Component({
  selector: 'app-list-projet',
  templateUrl: './list-projet.component.html',
  styleUrls: ['./list-projet.component.scss']
})
export class ListProjetComponent implements OnInit {
    panelOpenState: boolean;
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three'];
    auto: any;
     projects=[];
    private search: any;
    private length: any;
    private numtache: number;
    private numchat: number;
    private numreunion: number;


    constructor(public alertController: AlertController,private service:DevService,private auth:AuthenticateService) {
      this.service.getProject(this.auth.userId()).snapshotChanges().subscribe( res=>{
          this.projects=(JSON.parse(JSON.stringify(res)));
          console.log(this.projects);
this.search=this.projects;
this.length=this.projects.length;
console.log(this.projects)
this.service.getProjectclient('')
      });

this.service.getProjectclient("");



  }

    filterItem(){
        if(!this.myControl.value){
            this.projects=this.search;
        } // when nothing has typed
        // @ts-ignore
        this.projects = Object.assign([], this.search).filter(
            item => item.payload.nameProject.toLowerCase().indexOf(this.myControl.value.toLowerCase()) > -1
        )
    }
  ngOnInit() {
  }
delete(id){
      this.service.deleteProjet(id).then(success=>{
          Swal.fire({
              title: 'delete',
              text: ' Success!',
              icon: 'success',
          })
      })
}

    async alert(id) {
        const alert = await this.alertController.create({
            header: 'delete',
            message: 'This is an alert message.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (res) => {

                    }
                }, {
                    text: 'Okay',
                    handler: () => {

this.delete(id)                    }
                }
            ]
        });


        await alert.present();
    }

    count(projet: any) {
        this.numtache=  (projet.payload.taches)? Object.keys(projet.payload.taches).length:0
        this.numchat=(projet.payload.taches)?Object.keys(projet.payload.taches).length:0
        this.numreunion=(projet.payload.taches)?Object.keys(projet.payload.taches).length:0

    }
}

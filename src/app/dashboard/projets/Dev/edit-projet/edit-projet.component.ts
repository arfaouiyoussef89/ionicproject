import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import capture from '@ionic/pro/dist/src/services/monitoring/capture';
import Swal from 'sweetalert2';
import {NavController} from '@ionic/angular';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';

@Component({
  selector: 'app-edit-projet',
  templateUrl: './edit-projet.component.html',
  styleUrls: ['./edit-projet.component.scss']
})
export class EditProjetComponent implements OnInit {
    private editProject : FormGroup;
    private id: any;
    private dev: any;
    private clients: any;

    constructor( private nav:NavController,private formBuilder: FormBuilder,private route:ActivatedRoute,private service: ChefProjetService) {
        this.id = this.route.snapshot.params.id;
        this.service.getAllDev().pipe().subscribe((res:any)=>{
            this.dev=(JSON.parse(JSON.stringify(res)))
        }  );
        this.service.getAllClients().subscribe(res=>{
            this.clients=(JSON.parse(JSON.stringify(res)));
        }  );
        this.editProject = this.formBuilder.group({
            nameProject: ['', Validators.required],
            description: [''],
            client: ['', Validators.required],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],

            team: ['', Validators.required],



        });
    }
    logForm(){
        this.service.updateProjet(this.id,this.editProject.value).then(()=>{

            Swal.fire({
                title: 'update',
                text: 'sucess!',
                icon: 'success',
            })

            this.nav.back();
        })
    }


    ngOnInit() {

    }
}

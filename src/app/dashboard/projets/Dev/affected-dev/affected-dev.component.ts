import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-affected-dev',
  templateUrl: './affected-dev.component.html',
  styleUrls: ['./affected-dev.component.scss']
})
export class AffectedDevComponent implements OnInit {
    panelOpenState: boolean;
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three'];
    auto: any;
    private id: any;
    private taches: any;
private list:any;
    private devs: any[];
    private tabs= [];
    private p=[];
    private idTache: any;
    constructor(public alertController: AlertController,private service:ChefProjetService,private route:ActivatedRoute,private router:Router) {
        this.id = this.route.snapshot.params.ids;
        this.idTache = this.route.snapshot.params.id;
        this.list=(JSON.parse(localStorage.getItem("dev-tache")));
console.log(this.route.snapshot.params)

Object.keys(this.list).map(item=>{
   console.log("item")
    this.service.getDevById(item).pipe().subscribe((res)=>{
        this.p.push({["key"]:item});

res.forEach(item=>{

         this.p.push({[item.key]:item.payload.val()});


});
        let object = Object.assign({}, ...this.p);


        this.tabs.push(object)
    });

});
console.log("tab")
console.log(this.tabs);

this.service.getTache(this.id).subscribe(res=>{
            this.taches=(JSON.parse(JSON.stringify(res)));

        })
    }


    ngOnInit() {

    }

    async alert(id) {
        console.log(id)
        const alert = await this.alertController.create({
            header: 'delete',
            message: 'This is an alert message.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (res) => {

                    }
                }, {
                    text: 'Okay',
                    handler: () => {
                 this.service.deleteTeam(this.id,this.idTache,id).then(()=>{

this.tabs=this.tabs.filter(item=>item.key!=id);                     console.log(id);
                 })                   }
                }
            ]
        });


        await alert.present();
    }


}

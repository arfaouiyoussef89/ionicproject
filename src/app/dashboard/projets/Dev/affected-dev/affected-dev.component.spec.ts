import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectedDevComponent } from './affected-dev.component';

describe('AffectedDevComponent', () => {
  let component: AffectedDevComponent;
  let fixture: ComponentFixture<AffectedDevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectedDevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectedDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

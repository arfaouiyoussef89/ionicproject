import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-list-tache',
  templateUrl: './list-tache.component.html',
  styleUrls: ['./list-tache.component.scss']
})
export class ListTacheComponent implements OnInit {
    panelOpenState: boolean;
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three'];
    auto: any;
    private id: any;
    private taches=[];
    isaffected: any;
    private search: any;

    constructor(private alertController:AlertController,private service:ChefProjetService,private route:ActivatedRoute,private router:Router) {
        this.id = this.route.snapshot.params.id;

        this.service.getTache(this.id).subscribe(res=>{
            this.taches=(JSON.parse(JSON.stringify(res)));
this.search=this.taches;
        })
    }
affectedTache(){
        this.isaffected=true
}
    notaffectedTache(){
        this.isaffected=false
    }

    listDev(key,val:any)
    {


localStorage.setItem("dev-tache",JSON.stringify(val));

    this.router.navigate(["./"+key+"/affected-dev"],{ relativeTo: this.route });
    }
    filterItem(){
        if(!this.myControl.value){
            this.taches=this.search;
        } // when nothing has typed
        // @ts-ignore
        this.taches = Object.assign([], this.search).filter(
            item => item.payload.nameTache.toLowerCase().indexOf(this.myControl.value.toLowerCase()) > -1
        )}
    ngOnInit() {

    }
    async alert(id) {
        const alert = await this.alertController.create({
            header: 'delete',
            message: 'This is an alert message.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (res) => {

                    }
                }, {
                    text: 'Okay',
                    handler: () => {

                        this.service.deleteTacheById(this.id,id).then()
                                           }
                }
            ]
        });
        await alert.present();
    }

}

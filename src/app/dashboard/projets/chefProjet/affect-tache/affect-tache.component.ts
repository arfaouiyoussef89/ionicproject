import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {NavController} from '@ionic/angular';
import Swal from "sweetalert2";

@Component({
  selector: 'app-affect-tache',
  templateUrl: './affect-tache.component.html',
  styleUrls: ['./affect-tache.component.scss']
})
export class AffectTacheComponent implements OnInit {
    private affectTache : FormGroup;
    private id: any;
    private tacheId: any;
    private dev: any;
    private clients: any;

    constructor( private nav:NavController,private formBuilder: FormBuilder,private route:ActivatedRoute,private service:ChefProjetService) {
        this.id = this.route.snapshot.params.ids;
        this.tacheId=this.route.snapshot.params.id;
        this.service.getAllDev().pipe().subscribe((res:any)=>{
            this.dev=(JSON.parse(JSON.stringify(res)))
        }  );
        this.service.getAllClients().subscribe(res=>{
            this.clients=(JSON.parse(JSON.stringify(res)));
        }  );

        this.affectTache = this.formBuilder.group({
            devId: ['', Validators.required],



        });
    }
    logForm(){
        let team=[];
        let  teamArray=[];
        team=this.affectTache.controls["devId"].value;
        team.map(res=>{
            teamArray[res]=true;
        });
        this.affectTache.controls["devId"].setValue(teamArray);
console.log(this.affectTache.value)
        this.service.affectTache(this.id,this.tacheId,this.affectTache.value);


            Swal.fire({
                title: 'Affected ',
                text: 'success!',
                icon: 'success',
            });
            this.nav.back()


        
    }


    ngOnInit() {


    }

}

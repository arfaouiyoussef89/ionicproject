import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';
import {ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-add-chat-room',
  templateUrl: './add-chat-room.component.html',
  styleUrls: ['./add-chat-room.component.scss']
})
export class AddChatRoomComponent implements OnInit {

    private addProject : FormGroup;
    clients:any;
    dev:any;
    name: any;
    projects:any;
    private list=[];
    nameError: any;
    project:any;
    private id: any;

    constructor( private nav:NavController,private formBuilder: FormBuilder,private route:ActivatedRoute,private service:ChefProjetService) {
        this.id = this.route.snapshot.params.id;
        this.addProject = this.formBuilder.group({
            roomName: ['', Validators.required],
            description: [''],
            client: ['', Validators.required],

            team: ['', Validators.required],



        });

        this.service.getAllDev().pipe().subscribe((res:any)=>{
            this.dev=(JSON.parse(JSON.stringify(res)))
            console.log(this.dev)

        }  );
        this.service.getAllClients().subscribe(res=>{
            this.clients=(JSON.parse(JSON.stringify(res)));
            console.log(this.clients)
        }  );
    }
    logForm() {
        let  exists=null;
        let team=[];
        let arrayTeam=[];
team=this.addProject.controls["team"].value;
team.map(res=>{
    arrayTeam[res]=true;

});
this.addProject.controls["team"].setValue(arrayTeam);
                this.service.addChatRoom(this.id,this.addProject.value).then(res=>{

                        Swal.fire({
                        title: 'add chatroom  ',
                        text: 'success',
                        icon: 'success',
                    });
        this.nav.back()}

        );



    }






    ngOnInit() {

    }

}

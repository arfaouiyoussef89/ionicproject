import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StateProjectComponent} from './state-project.component';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
const routes: Routes = [
    {
        path: '',
        component: StateProjectComponent

    }
];

@NgModule({
  declarations: [StateProjectComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        IonicModule,
        FormsModule
    ]
})
export class StateProjectModule { }

import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Chart } from "chart.js";
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';

@Component({
  selector: 'app-state-project',
  templateUrl: './state-project.component.html',
  styleUrls: ['./state-project.component.scss']
})
export class StateProjectComponent implements OnInit {


    // @ts-ignore
    @ViewChild('barCanvas') barCanvas: ElementRef;
    // @ts-ignore
    @ViewChild("doughnutCanvas") doughnutCanvas: ElementRef;

    private barChart: Chart;
    private doughnutChart: Chart;
    private lineChart: Chart;
    private projects: any[];
    key: any;
    private projet: any;
    private numberTache: number;
    private numberChatRoom: number;
    private numberReunion: number;
    private count: any;

    constructor(private service:ChefProjetService,private auth:AuthenticateService) {
        this.service.getProject(this.auth.userId()).snapshotChanges().subscribe((res:any)=>{
this.projects=JSON.parse(JSON.stringify(res))});


    }

    ngOnInit() {


    }

    get()
    {
this.service.getProjectById(this.key).on("value",res=>{
this.projet=res.val();
    console.log(res.val())
     this.numberTache=Object.keys(this.projet.taches).length?Object.keys(this.projet.taches).length:0;
    this.numberChatRoom=(this.projet.chatRooms)?Object.keys(this.projet.chatRooms).length:0;
    this.numberReunion=(this.projet.chatRooms)?Object.keys(this.projet.chatRooms).length:0;
    let t=this.projet.taches;
    let arr = [];
    this.count=0
    Object.values(t).map((res:any)=>{
        if(res.progress===100)
        {
            this.count=this.count+1
        }});
    this.barChart = new Chart(this.barCanvas.nativeElement, {
        type: "bar",
        data: {
            labels: ["Tache", "Chatroom", "Reunion"],
            datasets: [
                {
                    label: "# number ",
                    data: [this.numberTache, this.numberChatRoom, this.numberReunion],
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.2)",
                        "rgba(54, 162, 235, 0.2)",
                        "rgba(255, 206, 86, 0.2)",
                        "rgba(75, 192, 192, 0.2)",
                        "rgba(153, 102, 255, 0.2)",
                        "rgba(255, 159, 64, 0.2)"
                    ],
                    borderColor: [
                        "rgba(255,99,132,1)",
                        "rgba(54, 162, 235, 1)",
                        "rgba(255, 206, 86, 1)",
                        "rgba(75, 192, 192, 1)",
                        "rgba(153, 102, 255, 1)",
                        "rgba(255, 159, 64, 1)"
                    ],
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            }
        }
    });

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
        type: "doughnut",
        data: {
            labels: ["Total Taches", "Tache done"],
            datasets: [
                {
                    label: "# of Votes",
                    data: [this.numberTache, this.count],
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.2)",
                        "rgba(54, 162, 235, 0.2)",

                    ],
                    hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
                }
            ]
        }
    });


})
    }
}

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ChefProjetService} from '../../../../../services/chefprojet/chef-projet.service';
import {AuthenticateService} from '../../../../../services/auth/authentication.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-add-reunion',
  templateUrl: './add-reunion.component.html',
  styleUrls: ['./add-reunion.component.scss']
})
export class AddReunionComponent implements OnInit {
    clients:any;
    dev:any;
    name: any;
    projects:any;
    private list=[];
    nameError: any;
    project:any;
    private id: any;
    addReunion: FormGroup;
    constructor( private formBuilder: FormBuilder,private route:ActivatedRoute,private service:ChefProjetService,private auth:AuthenticateService) {

            this.id = this.route.snapshot.params.id;

            this.addReunion = this.formBuilder.group({
            description: [''],
            client: [''],
            startDate: ['', Validators.required],

            team: ['', Validators.required],



        });

        this.service.getAllDev().pipe().subscribe((res:any)=>{
            this.dev=(JSON.parse(JSON.stringify(res)))
        }  );
        this.service.getAllClients().pipe().subscribe(res=>{
            this.clients=(JSON.parse(JSON.stringify(res)));
            console.log(this.clients)
        }  );
    }
    logForm() {
        let team=[];
        let teamArray=[];
        team=this.addReunion.controls["team"].value;
        team.map(res=>{
            teamArray[res]=true;
        });
        this.addReunion.controls['team'].setValue(teamArray);
        this.service.addReunion(this.id,this.addReunion.value)


    }
    test(){
        let exists=null;


        this.service.getProject(this.auth.userId()).valueChanges().subscribe((res)=>{

            exists=res.filter((item:any)=> item.nameProject === name );
            console.log(exists.length);

            if(exists.length > 0 )
            {

                this.nameError="project name already exists"
            }

        });



    }

    ngOnInit() {

    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { SettingsPage } from './settings.page';
import {AvatarModule} from 'ngx-avatar';
import {MatButtonModule} from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: SettingsPage

  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        AvatarModule,
        ReactiveFormsModule,
        MatButtonModule
    ],
  declarations: [SettingsPage]
})
export class SettingsPageModule {}

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import capture from '@ionic/pro/dist/src/services/monitoring/capture';
import Swal from 'sweetalert2';
import {NavController} from '@ionic/angular';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';

@Component({
  selector: 'app-edit-projet',
  templateUrl: './edit-projet.component.html',
  styleUrls: ['./edit-projet.component.scss']
})
export class EditProjetComponent implements OnInit {
    private editProject : FormGroup;
    private id: any;
    private dev: any;
    private clients: any;

    constructor( private auth:AuthenticateService,private nav:NavController,private formBuilder: FormBuilder,private route:ActivatedRoute,private service: ChefProjetService) {
        this.id = this.route.snapshot.params.id;
        this.service.getAllDev().pipe().subscribe((res:any)=>{
            this.dev=(JSON.parse(JSON.stringify(res)))
        }  );
        this.service.getAllClients().subscribe(res=>{
            this.clients=(JSON.parse(JSON.stringify(res)));
        }  );
        this.editProject = this.formBuilder.group({
            nameProject: ['', Validators.required],
            description: [''],

            client: ['', Validators.required],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],

            team: ['', Validators.required],



        });
    }
    logForm(){
        let t=[];
        let s=[];

        t=this.editProject.controls["team"].value;

        t.map(res=>{
            s[res.toString().trim()]=true;
        });
        this.editProject.controls['team'].setValue(s);
     console.log(this.editProject.value);
        this.editProject.addControl('chefprojet', new FormControl('', Validators.required));

        this.editProject.controls["chefprojet"].setValue(this.auth.userId());
          this.service.updateProjet(this.id,this.editProject.value).then(()=>{

            Swal.fire({
                title: 'update',
                text: 'success!',
                icon: 'success',
            })

            this.nav.back();
        })
    }


    ngOnInit() {

        this.service.getProjectById(this.id).once("value")
            .then(snapshot =>{
let res=snapshot.val();
                this.editProject.setValue({


                    nameProject: !!res.nameProject? res.nameProject:'',
                    description: !!res.description? res.description:'',
                    client: !!res.client? res.client:'',
                    startDate: !!res.startDate? res.startDate:'',
                    endDate:!!res.endDate? res.endDate:'',

                    team: !!res.team? res.team:'',




                })


            })
            .catch(error => ({
                errorCode: error.code,
                errorMessage: error.message
            }));

    }
    goBack()
    {
        this.nav.back();
    }

}

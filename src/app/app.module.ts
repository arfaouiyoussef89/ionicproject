import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Camera } from '@ionic-native/camera/ngx';

// Modal Pages

// Components
import { NotificationsComponent } from './components/notifications/notifications.component';

import { AuthenticateService } from './services/auth/authentication.service';
import { AngularFireAuthModule } from '@angular/fire/auth/auth.module';

import * as firebase from 'firebase';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestore, AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireDatabase, AngularFireDatabaseModule} from '@angular/fire/database';
import { AuthGuardService} from './auth/auth-guard.service';
import {environment} from '../environments/environment';
import {AngularFireAuth} from '@angular/fire/auth';
import { IonicStorageModule } from '@ionic/storage';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {LoginActivate} from './auth/loginActivate';
import {AngularFireStorage} from '@angular/fire/storage';

firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [AppComponent, NotificationsComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
       SweetAlert2Module.forRoot(),
      AngularFirestoreModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule ,
    HttpClientModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
  ],
  entryComponents: [NotificationsComponent],
  providers: [
    StatusBar,
    SplashScreen,LoginActivate,AngularFireStorage,
    AuthenticateService,Camera,AuthGuardService,
      AngularFireAuth,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
